/*
  Name: DynamicArray.cpp--implementing the member functions for our array-based DynamicArray ADT
  Copyright: Frank Dye, 11/13/13
  Author: Frank Dye, ID: 912927332
  Date: 11/13/13
  Description: Implementation of our array-based DynamicArray ADT
*/

#include <iostream>
#include "DynamicArray.h"  // header file



DynamicArray::DynamicArray() 
{

   //Allocate our DynamicArray
   try 
   {
	   items = new DynamicArrayItemType[DEFAULT_ARRAY_SIZE];
   }
   catch (bad_alloc& e)
   {
	   cerr << "Cannot allocate memory: " << e.what() << '\n';
   }

   //Initialize items array to 0
	for (int i = 0 ; i < DEFAULT_ARRAY_SIZE; i++)
	   items[i] = 0;

	//Record our new allocated size
	allocated_size = DEFAULT_ARRAY_SIZE;

}  // end default constructor


DynamicArray::DynamicArray(const DynamicArray& anArray)
{
	(*this).allocated_size = anArray.allocated_size;

	(*this).items = new DynamicArrayItemType[(*this).allocated_size];

	for (int i = 0; i < (*this).allocated_size; i++)
		(*this).items[i] = anArray.newRetrieve(i);
}

DynamicArray::~DynamicArray() //Default Destructor
{
	//delete our dynamic array
	delete [] items;

}

int DynamicArray::getLength() const //Get length is changed to return the maximum index of our dynamic array
									//Not how many items are stored within
{
   return allocated_size;
}  // end getLength

//Index starts and 0, no translate function
void DynamicArray::remove(int index)
	   throw(DynamicArrayIndexOutOfRangeException, bad_alloc)
{

	DynamicArrayException DynamicArrayEx("DynamicArrayException: DynamicArray full on insert");
	DynamicArrayIndexOutOfRangeException outOfRange("DynamicArrayIndexOutOfRangeException: Bad index on insert");   


	//First set items[index] to 0 if its within range
	if (index < allocated_size && index>= 0)
	{
		//Index is okay, so zero it
		items[index] = 0;
	}
	else
	{
		//Throw out of range exception, alternatively we could grow the array, but this doesn't make a ton of sense
		//in the context of remove, so its probably better to let the user know something weird is happening.
		throw outOfRange;
	}

	//Check to see if we can shrink the array
	shrinkArray();


}//End Remove


void DynamicArray::shrinkArray()
{
	
	//Now check to see if there is unused space and we can shrink the array
	int largestUsedIndex = 0;
	int defaultArrayBoundary = 0;

	//Find the largest index that is used in our array
	for (int i = 0; i < allocated_size; i++)
	{
		if (items[i] != 0)
		{
			largestUsedIndex = i;
	
		}
	}



	defaultArrayBoundary = ceil(largestUsedIndex / DEFAULT_ARRAY_SIZE) + 1; //Always has at least 1 multiple of DEFAULT_ARRAY_SIZE

	if (defaultArrayBoundary < (allocated_size / DEFAULT_ARRAY_SIZE))
	{
		//We have unused space so shrink the array
		DynamicArrayItemType* newPtr = NULL;

		//Calculate our new memory block size
		int newAllocSize = (defaultArrayBoundary) * DEFAULT_ARRAY_SIZE;

		try 
		{
			//Allocated a new block of memory 
			newPtr = new DynamicArrayItemType[newAllocSize];
		}
		catch (bad_alloc& e)
		{
			cerr << "Cannot allocate memory: " << e.what() << '\n';
		}

		//Zero our new block of memory
		for (int i = 0 ;i < newAllocSize; i++)
			newPtr[i] = 0;

		for (int i = 0; i < newAllocSize; i++)
			newPtr[i] = items[i];

		//Delete items
		delete [] items;

		//Change items to newPtr
		items = newPtr;
		allocated_size = newAllocSize;
	}
	//At this point we have zero'd the index and shrunk the array if appropriate, were done
}

//Index starts at 0, using translate was silly.
void DynamicArray::insert(int index, const DynamicArrayItemType& newItem)
	throw(DynamicArrayIndexOutOfRangeException, DynamicArrayException, bad_alloc)
{
	DynamicArrayException DynamicArrayEx("DynamicArrayException: DynamicArray full on insert");
	DynamicArrayIndexOutOfRangeException outOfRange("DynamicArrayIndexOutOfRangeException: Bad index on insert");   

	//Changed this function to handle dynamic array sizing
	
	//Check to see if index is bigger then our allocated size
	if (index > allocated_size -1)
	{
		//We are trying to insert an item past what we have space for so resize our array
		
		//We resize our array in multiples of DEFAULT_ARRAY_SIZE so that hopefully this can be tuned to
		//optimize for the fewest resizes and most efficient use of memory

		
		int newArraySize = 0;
		DynamicArrayItemType* newPtr = NULL;


		newArraySize = (ceil(index / DEFAULT_ARRAY_SIZE) + 1) * DEFAULT_ARRAY_SIZE; //This should roundup to the nearest multiple of DEFAULT_ARRAY_SIZE

		try
		{
			newPtr = new DynamicArrayItemType[newArraySize];
		}
		catch (bad_alloc& e)
		{
			 cerr << "Cannot allocate memory: " << e.what() << '\n';
		}

		//Zero the array
		for (int i = 0; i < newArraySize; i++)
			newPtr[i] = 0;

		//Now copy the old array into the new one
		for (int i = 0; i < allocated_size; i++)
			newPtr[i] = items[i];

		//Delete old array
		delete [] items;

		//Change the pointers
		items = newPtr;

		//Adjust allocated_size
		allocated_size = newArraySize;
		
		//We are done with resizing
	}

	//At this point we know the DynamicArray was either big enough to take the new item or was grown to accomodate it 
	//so simply insert it

	items[index] = newItem;

}  // end insert




void DynamicArray::print() const
{
	//Print out the DynamicArray
	
	cout << "[ ";
	for (int i = 0; i < allocated_size; i ++)
	{
		cout << items[i] << " ";
	}

	cout << "]";
	cout << "\n";
}



//This is postfix
DynamicArray DynamicArray::operator++( int )
{
	DynamicArray* ptrDynamicArray = new DynamicArray;

	//Create DynamicArray that is the same to return
	for (int i = 0; i < allocated_size; i++)
		ptrDynamicArray->insert(i, items[i]);

	for (int i = 0; i < allocated_size; i++)
		items[i]++;

	//Return the original DynamicArray, then increment each item in the DynamicArray afterwards
	return *ptrDynamicArray;
}


//This is prefix
DynamicArray DynamicArray::operator++()
{
	DynamicArray* ptrDynamicArray = new DynamicArray;

	//Prefix, increment the DynamicArray and return it
	for (int i = 0; i < allocated_size; i++)
		ptrDynamicArray->insert(i, items[i] + 1);

	for (int i = 0; i < allocated_size; i++)
		items[i]++;

	return *ptrDynamicArray;
}

ostream &operator<<( ostream &out, const DynamicArray &L ) {
	out << "[ ";
	for (int i=0; i < L.getLength(); i++) 
	{
		out << L.newRetrieve(i) << ' ';
	}
	out << ']';
	return out;
}

DynamicArray DynamicArray::operator/( float f1 )
{
	int length = this->getLength();
	DynamicArray* ptrDynamicArray = new DynamicArray;

	//First check lengths match
	if (f1 != 0)
	{
		//Continue, same length
		for (int i = 0; i < length; i++)
		{
			float a = 0;
			a = this->newRetrieve(i);
			ptrDynamicArray->insert(i,a / f1);
		}

	}
	else
		throw DynamicArrayException("Division by Zero!");

	return *ptrDynamicArray;
}

DynamicArray DynamicArray::operator*( float f1 )
{
	int length = 0;
	
	length = this->getLength();
	DynamicArray* ptrDynamicArray = new DynamicArray;

	//Continue, same length
	for (int i = 0; i < length; i++)
	{
		ptrDynamicArray->insert(i,this->newRetrieve(i) * f1);
	}
	
	return *ptrDynamicArray;

}

DynamicArray DynamicArray::operator-( DynamicArray& A)
{
	int aLength = 0;
	int bLength = 0;
	aLength = A.getLength();
	bLength = this->getLength();

	DynamicArray* ptrDynamicArray = new DynamicArray;

	//First check lengths match
	if (aLength == bLength)
	{
		//Continue, same length
		for (int i = 0; i < aLength; i++)
		{
			ptrDynamicArray->insert(i,this->newRetrieve(i) - A.newRetrieve(i));
		}

	}
	else
		throw DynamicArrayException("Two DynamicArray's lengths do not match, cannot perform - operator");

	return *ptrDynamicArray;
}


//Overload the = operator 
DynamicArray DynamicArray::operator= (const DynamicArray& anArray)
{
	(*this).allocated_size = anArray.allocated_size;

	if ((*this).items != NULL)
		delete [] items;

	items = new DynamicArrayItemType[(*this).allocated_size];

	for (int i = 0; i < (*this).allocated_size; i++)
		(*this).items[i] = anArray.newRetrieve(i);

	return (*this);

}


//Overload the [] operator 
DynamicArrayItemType& DynamicArray::operator[] (int index)
{
	shrinkArray(); //First check to see if we can shrink our array

	//If our array is already big enough then simply retrieve a pointer to the array index
	if (index < allocated_size)
	{
		return (*this).newRetrieve(index);
	}
	else
	{
		//If not call insert which grows are array automatically, then return address of our array index
		DynamicArrayItemType zero = 0;
		insert(index, zero);
		return (*this).newRetrieve(index);
	}
}

DynamicArray DynamicArray::operator+( DynamicArray& A)
{
	int aLength = 0;
	int bLength = 0;
	aLength = A.getLength();
	bLength = this->getLength();

	DynamicArray* ptrDynamicArray = new DynamicArray;

	//First check lengths match
	if (aLength == bLength)
	{
		//Continue, same length
		for (int i = 0; i < aLength; i++)
		{
			ptrDynamicArray->insert(i,A.newRetrieve(i) + this->newRetrieve(i));
		}

	}
	else
		throw DynamicArrayException("Two DynamicArray's lengths do not match, cannot perform + operator");
	
	return *ptrDynamicArray;
}


//Test function to retrieve allocated size
int DynamicArray::getAllocatedSize()
{
	return allocated_size;
}

DynamicArrayItemType& DynamicArray::newRetrieve( int index ) const throw(DynamicArrayIndexOutOfRangeException)
{
	bool success;
	DynamicArrayIndexOutOfRangeException outOfRange;

	success = (index <= allocated_size) && (index >= 0);;

	if (!success) throw outOfRange;

	if (success)
		return items[index];

}
