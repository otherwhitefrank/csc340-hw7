/*
  Name: DynamicArrayExceptions.h
  Desc: declare two exception classes that are specific to DynamicArray
  Copyright: Frank Dye, 11/13/13
  Author: Frank Dye, ID: 912927332
  Date: 11/13/13
  Description: 
*/
#ifndef _DYNAMICARRAY_EXCEPTIONS_H
#define _DYNAMICARRAY_EXCEPTIONS_H

#include <stdexcept>
#include <string>

using namespace std;

class DynamicArrayIndexOutOfRangeException : public out_of_range
{
public:
   
   /**/
   DynamicArrayIndexOutOfRangeException(const string & message = "")
      : out_of_range(message.c_str())
   { }  // end constructor
   /**/
   
}; // end DynamicArrayIndexOutOfRangeException


class DynamicArrayException : public logic_error
{
public:
   
   /**/    
   DynamicArrayException(const string & message = "")
      : logic_error(message.c_str())
   { }  // end constructor
   /**/
};

#endif
