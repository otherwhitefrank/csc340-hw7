/*
  Name: main.cpp
  Copyright: Frank Dye, 11/11/2013
  Author: Frank Dye, ID: 912927332
  Date: 11/11/2013
  Description: A driver class to test out our DynamicArray class 
*/

#include <iostream>
#include "DynamicArray.h" // ADT NewVector operations

using namespace std;


int main()
{
   
	try 
	{
		DynamicArray a, b, c;
	   
		//Test Question 2, treat aList and bList as vectors
		cout << "Question 2: two vectors a and b" << endl;

		/*
		for (int i = 0; i < 100; i++)
			a.insert(i, i*2);

		for (int i = 0; i < 100; i++)
			b.insert(i, i*3);
		*/

		//Test out [] operator
		for (int i = 0; i < 100; i++)
		{
			a[i] = i*2;
			if (i % 20 == 0)
				cout << "\ni: " << i << " allocatedSize: " << a.getAllocatedSize() << endl;
		}


		for (int i = 0; i < 100; i++)
			b[i] = i*3;

		cout << "a: " << a << endl;
		cout << "b: " << b << endl;
	    
		//Test out remove to see if it can shrink the array
		for (int i = 99; i> 0; i--)
		{
			a[i] = 0;
			if (i % 20 == 0)
				cout << "\ni: " << i << " allocatedSize: " << a.getAllocatedSize() << endl;
		}

		cout << "a: " << a << endl;
		cout << "b: " << b << endl;

		//Fix a

		for (int i = 0; i < 100; i++)
		{
			a[i] = i*2;
		}

		c = b;


		cout << "b: " << b << endl;
		cout << "c: " << c << endl;

		cout << "a+b: " << a + b << endl;
		cout << "a-b: " << a - b << endl;
		cout << "a*4: " << a * 4.0 << endl;
		cout << "a/4: " << a / 4.0 << endl;
		cout << "++a: " << ++a << endl;
		cout << "a: " << a << endl;
		cout << "b++: " << b++ << endl;
		cout << "b: " << b;

	}
	catch( DynamicArrayException & e1)
	{
		//handling DynamicArrayException, e1.what() will returns whatever specific message and output it to standard error
		cerr << e1.what();
	}
	catch (DynamicArrayIndexOutOfRangeException & e2)
	{
		//handling DynamicArrayIndexOutOfRangeException
		cerr << e2.what();
	} 
	catch (...)
	{
		//handling all the other exceptions here
		cerr << "Additional Exceptions happened";
	}
	return 0;
}
