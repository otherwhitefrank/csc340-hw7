/*
  Name: DynamicArray.h--consisting of the interface of DynamicArray
  Copyright: Frank Dye, 11/11/2013
  Author: Frank Dye, ID: 912927332
  Date: 11/11/2013
  Description: A basic DynamicArray object using array-based implementation that can be used with indexes as a array. 
*/

/** @file DynamicArray.h */
#ifndef _DynamicArray_H
#define _DynamicArray_H

#include "DynamicArrayExceptions.h"

const int DEFAULT_ARRAY_SIZE = 20;
typedef float DynamicArrayItemType;

/** @class DynamicArray
 * ADT DynamicArray - Array-based implementation with exceptions */
class DynamicArray
{
public:
   DynamicArray(); //Make sure to create dynamic array
   DynamicArray(const DynamicArray& anArray);

   ~DynamicArray(); //Make sure to delete dynamic array

   /** @throw None. */
   bool isEmpty() const;

   /** @throw None. */
   int getLength() const;

   /** @throw DynamicArrayIndexOutOfRangeException  If index < 1 or index >
    *         getLength() + 1.
    *  @throw DynamicArrayException  If newItem cannot be placed in the DynamicArray
    *         because the array is full. */
   void insert(int index, const DynamicArrayItemType& newItem)
      throw(DynamicArrayIndexOutOfRangeException, DynamicArrayException);
   
   //Remove function, this will also check to see what the largest non zero value is and shrink the array
   //if there is more then 1 multiple of DEFAULT_ARRAY_SIZE free
   void remove(int index)
	   throw(DynamicArrayIndexOutOfRangeException, bad_alloc);

   //The other version of retrieve is stupid, this just returns a value when asked, very simple.
   DynamicArrayItemType& newRetrieve(int index) const
	   throw(DynamicArrayIndexOutOfRangeException);

   //My function to print the contents of the DynamicArray 
   void print() const;


   //Overloaded Operators
   //overload the + operator as a member function to add two arrays
	DynamicArray operator + (DynamicArray&);

	//overload the - operator as a member function to subtract one array from another
	DynamicArray operator - (DynamicArray&);

	//overload the * operator as a member function to multiply a array by a real number
	DynamicArray operator * (float f1);

	//overload the / as a member function to divide a array by a real number
	DynamicArray operator / (float f1);
		
	//overload the prefix and postfix ++ operators as two member functions to increment each element in a array by 1
	//This is prefix
	DynamicArray operator ++ ();

	//overload the prefix and postfix ++ operators as two member functions to increment each element in a array by 1
	//This is postfix
	DynamicArray operator ++ (int) ;

	//overload the << operator to print out a array 
	friend ostream &operator<<( ostream &out, const DynamicArray &L );

	//Overload the = operator 
	DynamicArray operator= (const DynamicArray& anArray);

	//Overload the [] operator 
	DynamicArrayItemType& operator[] (int index);

	//Test function to retrieve allocated size
	int getAllocatedSize();


private:
	//Helper function to shrink the array if applicable
	void shrinkArray(); 

	DynamicArrayItemType* items; //Pointer to our dynamic array

	 /** number of items in DynamicArray */
	int          allocated_size;

}; // end DynamicArray
// End of header file.

#endif
