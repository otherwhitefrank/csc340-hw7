CSC340-HW7
==========

CSC340: Programming Methodology with C++, HW7 focuses on dynamic data and memory allocation by implementing a dynamic 'list' ADT which grows and shrinks as data is added to it.
